package Programario;
    import java.sql.*;
public class Conexion {

    String host_Server="localhost";
    String user_Server="root";
    String pass_Server="";
    String db="programario_reuniones";
   /*
 
 */ 	
    boolean entro=false;
    Statement st=null;
    Connection con=null;
    
    public Conexion()
    {
        try
        {
            String driver1 = "com.mysql.cj.jdbc.Driver"; 
            Class.forName( driver1 );
            String url  = "jdbc:mysql://"+host_Server+"/"+db+"?useTimezone=true&serverTimezone=UTC";
            String user = user_Server;
            String pass = pass_Server;
            con = DriverManager.getConnection(url,user,pass);
            st= con.createStatement();  
            System.out.println("conectado");
            //	System.out.println ("Conectado a la Base de datos "+db);          
        }catch(Exception x21)
        {
        	javax.swing.JOptionPane.showMessageDialog(null,"Error codigo:\n"+x21);
            System.out.println(x21);
        }
    }
    
    public Connection getCon()
    {
    	return con;
    }
    public static void main (String[] args) {
		new Conexion();
	}

}